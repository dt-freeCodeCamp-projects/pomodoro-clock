const angular = require('angular');
import $ from 'jquery';
import '../node_modules/materialize-css/dist/css/materialize.min.css';
import '../node_modules/materialize-css/dist/js/materialize.min.js';
import Moment from 'moment';
import 'moment-timer';
import 'moment-duration-format';

angular.module('pomodoro', [])
  .controller('PomodoroController', ($scope, $interval) => {
    $scope.breakDisp = Moment.duration(5, "minutes").format();
    $scope.sessionDisp = Moment.duration(15, "minutes").format();
    $scope.breakNum = 5;
    $scope.sessionNum = 15;
    $scope.resDispText = "SESSION: ";
    $scope.resDispNum = Moment.duration(15, "minutes").format();
    $scope.progressBar = 0;
    $scope.progressColor = "green";
    $scope.clockStarted = false;
    $scope.clockPaused = false;
    $scope.playBeep = () => {
      document.getElementById("beep").play();
    }

    $scope.incNum = (num, type) => {
      if(type === "break") {
        $scope.breakNum = num === 59 ? num : num + 1;
        $scope.breakDisp = Moment.duration($scope.breakNum, "minutes").format();
        $scope.resDispText = "BREAK: ";
        $scope.resDispNum = Moment.duration($scope.breakNum, "minutes").format();
      } else if(type === "session") {
        $scope.sessionNum = num === 59 ? num : num + 1;
        $scope.sessionDisp = Moment.duration($scope.sessionNum, "minutes").format();
        $scope.resDispText = "SESSION: ";
        $scope.resDispNum = Moment.duration($scope.sessionNum, "minutes").format();
      }
    }

    $scope.decNum = (num, type) => {
      if(type === "break") {
        if(num > 1) {
          $scope.breakNum = num - 1;
        } else {
          $scope.breakNum = num;
        }
        $scope.breakDisp = Moment.duration($scope.breakNum, "minutes").format();
        $scope.resDispText = "BREAK: ";
        $scope.resDispNum = Moment.duration($scope.breakNum, "minutes").format();
      } else if(type === "session") {
        if(num > 1) {
          $scope.sessionNum = num - 1;
        } else {
          $scope.sessionNum = num;
        }
        $scope.sessionDisp = Moment.duration($scope.sessionNum, "minutes").format();
        $scope.resDispText = "SESSION: ";
        $scope.resDispNum = Moment.duration($scope.sessionNum, "minutes").format();
      }
    }

    $scope.start = (num, type) => {
      if ($scope.clockPaused) {
        $scope.clockPaused = false;
        $scope.timer = Moment.duration("00:" + $scope.resDispNum).timer({
          loop: false,
          start: true
        }, () => {
          // Callback
        });
      } else {
        $scope.clockStarted = true;
        let progressUnit = 0;
        let progressMeter = 0;
        $scope.timer = Moment.duration(num, "minutes").timer({
          loop: false,
          start: false
        }, () => {
          // Callback
        });
  
        if(type === "session") {
          $scope.resDispText = "SESSION: ";
          $scope.resDispNum = $scope.sessionDisp;
        } else if(type === "break") {
          $scope.resDispText = "BREAK: ";
          $scope.resDispNum = $scope.breakDisp;
        }
        $scope.resWatcher = $scope.$watch('resDispNum', (newValue, oldValue) => {
          if(type === "session") {
            progressUnit = 100 / ($scope.sessionNum * 60);
            progressMeter = progressMeter + progressUnit;
            $scope.progressColor = "green";
          } else if (type === "break") {
            progressUnit = 100 / ($scope.breakNum * 60);
            progressMeter = progressMeter + progressUnit;
            $scope.progressColor = "red";
          }
          $scope.progressBar = progressMeter;
        });
  
        $scope.timer.start();
        $scope.timerRefresh = $interval(() => {
          $scope.resDispNum = Moment($scope.timer.getRemainingDuration()).format("mm:ss");
          if(type === "session" && $scope.resDispNum === "00:00") {
            $scope.playBeep();
            $scope.timer.stop();
            $interval.cancel($scope.timerRefresh);
            $scope.start($scope.breakNum, "break");
          } else if(type === "break" && $scope.resDispNum === "00:00") {
            $scope.playBeep();
            $scope.timer.stop();
            $interval.cancel($scope.timerRefresh);
            $scope.start($scope.sessionNum, "session");
          }
        }, 50);
      }

    }
    $scope.stop = () => {
      $scope.timer.stop();
      $scope.clockPaused = true;
    };
    
    $scope.reset = () => {
      $scope.timer.stop();
      $interval.cancel($scope.timerRefresh);
      $scope.resWatcher();
      $scope.clockStarted = false;
      $scope.resDispText = "SESSION: ";
      $scope.resDispNum = Moment.duration($scope.sessionNum, "minutes").format();
      $scope.progressBar = 0;
      $scope.progressColor = "green";
    }
  });
